set table "main.wavespectrum.table"; set format "%.5f"
set samples 8000.0; plot [x=60:200] (2.5*(1.38571 - 0.00642857*x)**0.4)*sin(exp(0.03*x))
