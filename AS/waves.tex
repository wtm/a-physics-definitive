\defcatagory{Waves}

\begin{figure}%
  \figureruletop

  \contourlength{0.2em}%
  \centering\begin{tikzpicture}[yscale=1.5,xscale=1.9]
    \path[->, thick, draw] (-0.5, 0) -- (6.5, 0);
    \path[->, thick, draw] (0, -2) -- (0, 1.5);
    \begin{scope}
      \clip (-0.5, -1) rectangle (6.25, 1);
      \path[thin, draw, name path=wave] (-1, -1) cos (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0) sin (5, 1) cos (6, 0) sin (7, -1);
    \end{scope}
    \path[dashed, thin, draw, not] (3, 1) -- (5, 1);

    \path[thick, >={Triangle[]}, |<->|, draw] (0, -1.5) -- (4, -1.5) node[midway, label={below: \contour{white}{\refterm{wavelength}}}] {};
    \path[dashed, thin, draw] (4, -1.5) -- (4, 0);

    \path[thick, >={Triangle[]}, |<->|, draw] (1, -2.5) -- (5, -2.5) node[midway, label={below: \contour{white}{also \refterm{wavelength}}}] {};
    \path[dashed, thin, draw] (1, -2.5) -- (1, 1);
    \path[dashed, thin, draw] (5, -2.5) -- (5, 1);

    \path[name path=A] (5.7, 0) -- (5.7, 1);
    \path[name intersections={of=wave and A, by=B}];

    \path[thick, >={Triangle[]}, |<->|, draw] (1, 0) -- (1, 1) node[midway, label={[inner sep=0]right: \contour{white}{\refterm{amplitude}}}] {};
    \path[thick, >={Triangle[]}, |<->|, draw, not] (3, -1) -- (3, 1) node[midway, label={[inner sep=0, text=not]below right:\contour{white}{\NOT amplitude}}] {};
    \path[thin, ->, draw] (5.7, 0) -- (B) node[midway, label={[inner sep=0]right: \contour{white}{\refterm{displacement}}}] {};
  \end{tikzpicture}

  \caption{A sine wave.}

  \figureruletbottom
\end{figure}

\begin{question}%
  \qtitle{State what is meant by the \term{amplitude} of a wave}\qpoint{1}

  the maximum \refterm{displacement} \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Define the \term{wavelength} for a wave.}\qpoint{1}

  minimum distance between two points moving in phase \hfillpoint{1}\\
  \OR distance between neighbouring / consecutive peaks or troughs \hfillpoint{1}\\
  \OR distance moved by a wavefront in one \refterm{period} / one oscillation \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{State what is meant by the \term{displacement} of a wave}\qpoint{1}

  the distance from the equilibrium position / undisturbed position / midpoint / rest position \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{\termnoindex[frequency]{Frequency}\index{frequency}\ldots}

  Def: the number of wavefronts / crests / \refterm{wavelength} passing a (fixed) point on the wave per unit time \OR number of oscillations of the source per unit time.\\*
  \NOT something per \textit{second}. See the NOTs under \refterm{velocity} \\*
  \NOT the number of \textit{complete} oscillations per unit time since frequency is not necessary an integer value.

  Quantity: $\text{\refterm{period}}\ ^{-1}$

  Unit: \SI{}{Hz} = \SI{}{s^{-1}}

  Not to be confused with \refterm{period} or \refterm{wavelength}.
\end{question}

\begin{question}%
  \qtitle{\termnoindex[period]{Period}\index{period}\ldots}

  Def: time between adjacent wavefronts \OR time for one oscillation.

  Quantity: $\text{\refterm{frequency}}\ ^{-1}$

  Unit: \SI{}{s}

  Not to be confused with \refterm{frequency} or \refterm{wavelength}.
\end{question}

\begin{question}%
  \qtitle{State the difference between a \term{stationary wave} and a \term{progressive wave} in terms of\ldots}

  \begin{itemize}
    \item [(i)] the energy transfer along the wave:\qpoint{1}
      
      in a stationary wave \refterm{energy} is not transferred \OR in a progressive wave \refterm{energy} is transferred \hfillpoint{1}

    \item [(ii)] the phase of two adjacent vibrating particles:\qpoint{1}

      in a stationary wave (adjacent) particles are \refterm{in phase} \OR in a progressive wave (adjacent) particles are \refterm{out of phase}/have a \refterm{phase difference}/not in phase \hfillpoint{1}

    \item [(iii)] the amplitude of the particles' vibration:\qpoint{1}

      in a progressive wave all particles have same amplitude \OR in a stationary wave nodes have minimum / zero amplitude and antinodes have maximum amplitude
        (or simply `amplitude varies for stationary wave') \hfillpoint{1}
  \end{itemize}

  \textbf{Note}: `Progressive wave being formed by one / stationary wave being formed by two waves' is \NOT a difference and is actually not correct.
\end{question}

\begin{question}%
  \qtitle{State what is meant by an \term{antinode} of the \refterm{stationary wave}}\qpoint{1}

  Position where maximum \refterm{amplitude} \hfillpoint{1}

  (See figure~\ref{fig:stationary-wave})
\end{question}

\begin{figure}%
  \figureruletop

  \centering\begin{tikzpicture}[xscale=3.14159]
    \path[draw] (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0);
    \path[draw] (0, 0) sin (1, -1) cos (2, 0) sin (3, 1) cos (4, 0);
    \foreach \i in {-0.9,-0.6,-0.2,0.2,0.6,0.9} {
      \path[draw,yscale=\i,opacity=0.25,ultra thin] (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0);
    }
    \def\arrowdownto#1#2{
      \path[draw,thick,->] #1 ++(0, 0.6) -- ++(0, -0.5) node[pos=0,label={above:#2},inner sep=0,anchor=center] {}
    }
    \arrowdownto{(1, 1)}{\refterm{antinode}};
    \arrowdownto{(2, 0)}{\refterm{node}};
    \arrowdownto{(3, 1)}{\refterm{antinode}};
  \end{tikzpicture}

  \caption{\refterm{node} and \refterm{antinode}s in a \refterm{stationary wave}.}
  \label{fig:stationary-wave}

  \figureruletbottom
\end{figure}

\begin{question}%
  \qtitle{By reference to vibrations of the points on a wave and to its direction of energy transfer, distinguish between \term{transverse waves} and \term{longitudinal waves}.}\qpoint{2}

  Transverse waves have vibrations / displacement of particles that are perpendicular to the direction of energy travel / propagation \hfillpoint{1}\\*
  Longitudinal waves have vibrations / displacement of particles that are parallel to the direction of energy travel / propagation \hfillpoint{1}\\*
  \NOT direction of motion of the wave / wave travel
\end{question}

\begin{question}%
  \qtitle{State the conditions required for the formation of a \refterm{stationary wave}}\qpoint{2}

  (two) waves travelling (at same speed) in opposite directions overlap \hfillpoint{1}\\*
  waves (are same type and) have same \refterm{frequency}/\refterm{wavelength} \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Describe the features that are seen on the stretched string that indicate \refterm{stationary wave}s have been produced.}\qpoint{1}

  points on string have different \textbf{amplitudes} varying from maximum to zero/minimum \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain how \refterm{stationary wave}s are formed in a tube with one end closed / with a \refterm{microwave} source and a metal reflector (figure~\ref{fig:stat-metal-reflect}).}\qpoint{3}

  waves \textbf{from source} (e.g.\ loudspeaker) (travel down tube and) are reflected at closed end / reflector \hfillpoint{1}\\*
  two waves (travelling) in opposite directions with same \refterm{frequency}/\refterm{wavelength} and \refterm{speed} \hfillpoint{1}\\*
  \reftermnoindex[superposition]{overlap} \hfillpoint{1}
\end{question}

\begin{figure}[h]%
  \figureruletop

  \begin{tikzpicture}[thick]
    \coordinate (source) at (2, 0);
    \coordinate (detector) at (9, 0);
    \coordinate (reflector) at (13, 0);

    \path[fill, opacity=0.5] (0, 0) rectangle (15, -1);
    \path[draw] (0, 0) -- (15, 0);

    \path[draw] (source) rectangle +(1.5, 2) node [anchor=center] (tip) {} node[midway, anchor=center] {source};
    \path[draw] ($(tip) - (0, 0.2em)$) +(0, -0.2em) -- +(1em, 0) -- +(1em, -1em) -- +(0, -0.8em);

    \begin{scope}[shift=(detector)]
      \path[draw] (-0.5, 0.2) rectangle (0.5, 0);
      \path[draw] (-0.05, 0.2) -- (-0.05, 1.6) -- (0.05, 1.6) -- (0.05, 0.2);
      \path[draw, fill=white] (0, 1.6) circle[radius=0.5em] node[above=1em] {detector $D$};
    \end{scope}

    \begin{scope}[shift=(reflector)]
      \path[draw, ultra thick] (0, 0) -- (0, 2) node[pos=0.7, anchor=center] (a) {};
      \path[draw, thick] (a.center) -- (1, 3) node[pos=1, above right] {metal reflector};
    \end{scope}
  \end{tikzpicture}

  \caption{Figure for question~\ref{q:stat-metal-reflect}}
  \label{fig:stat-metal-reflect}

  \figureruletbottom
\end{figure}

\begin{question}%
  \label{q:stat-metal-reflect}%
  \qtitle{Explain how $D$ is used to show that stationary waves are formed between reflector and wave source in figure~\ref{fig:stat-metal-reflect}}\qpoint{2}

  detector/$D$ is moved between reflector and source\hfillpoint{1}\\*
  maximum, minimum/zero, (maximum\ldots\ etc.) observed on meter\allowbreak/deflections\allowbreak/readings\allowbreak/measurements\allowbreak/recordings\hfillpoint{1}\\*
  \NOT \refterm{node}s and \refterm{antinode}s observed.\\*
  \AVOID \refterm{amplitude} varies. The wave on the screen goes big and small and big and small\ldots\ is what's seen, going on to state things about \refterm{amplitude} is
  not a direct observation.
\end{question}

\begin{question}%
  \qtitle{Describe the \term{Doppler effect}}\qpoint{1}

  \textbf{observed} \refterm{frequency} is different to source \refterm{frequency} when source moves relative to observer \hfillpoint{1}\\*
  \NOT due to change in position of source
\end{question}

\begin{question}%
  \qtitle{(In a \refterm{Doppler effect} question) describe and explain the variation in the \refterm{frequency} of the sound heard by the observer.}\qpoint{$n$}

  \reftermnoindex[frequency]{Frequency}\index{frequency} increases / decreases \point{1} as observer moves toward / away from source (depending on question) \point{1}
  by $n$ \SI{}{Hz} \point{1}

  \OR

  \textbf{Repeated} rise and fall in \refterm{frequency} \point{1} caused by observer moving toward and away from the source \point{1}
  from $x$ \SI{}{Hz} to $y$ \SI{}{Hz} \point{1}.

  \textbf{Note:} if asked to describe ``qualitatively'', no need for $x$ \SI{}{Hz}. Moving toward source $\Rightarrow$ increase in \refterm{frequency},
  moving away from source $\Rightarrow$ decrease in \refterm{frequency}.
\end{question}

\begin{question}%
  \qtitle{Describe what is meant by a \term{polarised wave}}\qpoint{2}

  vibrations are in a single direction \hfillpoint{1}\\*
  applies to transverse waves \OR normal to direction of wave energy travel / propagation \hfillpoint{1}\\*
  \NOT vibration in only one plane
\end{question}

\begin{question}%
  \qtitle{State what is meant by \term{interference} / \term{superposition}}\qpoint{2}

  when two (or more) waves \refterm{superpose}/meet/overlap \hfillpoint{1}\\*
  resultant displacement is the sum of the displacements of each wave \hfillpoint{1}\\*

  \begin{framed}
    {\begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thin, dashed, draw] (-0.1, 0) -- (4.2, 0);
      \path[thick, draw] (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture} \hfill $+$ \hfill \begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thin, dashed, draw] (-0.1, 0) -- (4.2, 0);
      \path[thick, draw] (0, 0) sin (1, -1) cos (2, 0) sin (3, 1) cos (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture} \hfill $=$ \hfill \begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thick, draw] (0, 0) -- (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture}}

    \vspace{\baselineskip}\centerline{\term{destructive interference}}
  \end{framed}

  \begin{framed}
    {\begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thin, dashed, draw] (-0.1, 0) -- (4.2, 0);
      \path[thick, draw] (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture} \hfill $+$ \hfill \begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thin, dashed, draw] (-0.1, 0) -- (4.2, 0);
      \path[thick, draw] (0, 0) sin (1, 1) cos (2, 0) sin (3, -1) cos (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture} \hfill $=$ \hfill \begin{tikzpicture}[baseline={(0,-1mm)}]
      \path[thin, dashed, draw] (-0.1, 0) -- (4.2, 0);
      \path[thick, draw] (0, 0) sin (1, 2) cos (2, 0) sin (3, -2) cos (4, 0) node[pos=1, anchor=center] (a) {};
    \end{tikzpicture}}

    \centerline{\term{constructive interference}}
  \end{framed}
\end{question}

\begin{question}%
  \qtitle{Use the principle of \refterm{superposition} to explain \textit{<some observation>}}\qpoint{2}

  The waves (that overlap) have \refterm{phase difference} of $x \si{\degree}$ / $y\ \si{rad}$ / path difference of $z \lambda$ / $0$ / are \refterm{in phase} (depend on question) \hfillpoint{1}\\*
    constructive (if in phase) / destructive (if phase difference = \SI{180}{\degree}) interference \OR displacement larger / smaller (depend on question) \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{State what is meant by the \term{diffraction} of a wave.}\qpoint{2}

  When wave incident on/passes by/through an aperture/edge \hfillpoint{1}\\*
  it spreads (into the geometrical shadow) \hfillpoint{1}\\*
  \NOT bending\\*
  \NOT when the wave passes through an obstacle (because it will not get through if there is no edge. It reflects.)
\end{question}

\begin{question}%
  \qtitle{Explain the meaning of \term{coherent}}\qpoint{1}

  constant \refterm{phase difference} \hfillpoint{1}

  (If two waves have the same speed, you can't be coherent if the frequencies are different.)
\end{question}

\begin{question}%
  \qtitle{Explain the part played by \refterm{diffraction} in the production of the fringes in the \refterm{double slit experiment}}\qpoint{2}

  waves at (each) slit/aperture spread (into the geometric shadow) \hfillpoint{1}\\*
  (the spread) wave(s) overlap/\refterm[superposition]{superpose}/sum/meet/intersect \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain the reason why a double slit is used rather than two separate sources of light in the \refterm{double slit experiment}}\qpoint{1}

  two separate light sources are not in constant phase difference/\reftermnoindex[coherent]{coherence}\index{coherent}~\hfill\\*
  \OR waves/light from the double slit are \refterm{coherent}/have a constant phase difference \hfillpoint{1}
\end{question}

\begin{figure}[h]%
  \figureruletop

  \contourlength{0.2em}%
  \centering\begin{tikzpicture}[baseline=(baseline.base)]
    \def\lightarrow#1{\path [draw, -{Stealth}, thick] (0, #1) -- (3, #1)}
    \lightarrow{1};
    \lightarrow{0.5}
      node[pos=1, anchor=base west, xshift=1em] (slitline1) {};
    \lightarrow{0}
      node[midway, anchor=east, align=right, inner sep=0] (baseline) {\contour{white}{laser light}}
      node[pos=1, anchor=base west, xshift=1em] (centerline) {};
    \lightarrow{-0.5}
      node[pos=1, anchor=base west, xshift=1em] (slitline2) {};
    \lightarrow{-1};

    \path [draw=black, fill=gray, thick] ([yshift=0.25em, xshift=-0.25em]slitline1) rectangle ([yshift=3.25em, xshift=0.25em]slitline1) node[pos=1, anchor=west] (top) {};
    \path [draw=black, fill=gray, thick] ([yshift=-0.25em, xshift=-0.25em]slitline1) rectangle ([yshift=0.25em, xshift=0.25em]slitline2);
    \path [draw=black, fill=gray, thick] ([yshift=-0.25em, xshift=-0.25em]slitline2) rectangle ([yshift=-3.25em, xshift=0.25em]slitline2) node[pos=1, anchor=west] (bottom) {};
    \path [draw=black, thick, dashed] ([xshift=0.5em]centerline) -- ++ (12em, 0) node[pos=1, anchor=base west] (lineright) {};

    \def\letterbox#1{\makebox[1em][l]{#1}}

    \path [draw, thick] let \p1 = (lineright), \p2 = (top), \p3 = (bottom) in (\x1, \y2) -- (\x1, \y3)

      node[midway, right, align=left] {\letterbox{$X$} (centeral bright fringe)}
      node[midway, anchor=center, shape=circle, inner sep=0.2em, fill=black, draw=none] {}

      node[pos=0.25, right, align=left] {\letterbox{$Y$} (a dark fringe)}
      node[pos=0.25, anchor=center, shape=circle, inner sep=0.2em, fill=gray, draw=none] {}

      node[pos=0.75, right, align=left] {\letterbox{$Z$} (another dark fringe)}
      node[pos=0.75, anchor=center, shape=circle, inner sep=0.2em, fill=gray, draw=none] {};
  \end{tikzpicture}

  \caption{for question~\ref{q:double-slits-q1} and~\ref{q:double-slits-q2}. Not to scale.}
  \label{fig:double-slits-x}
  \figureruletbottom
\end{figure}

\begin{question}%
  \label{q:double-slits-q1}%
  \qtitle{Explain why a bright fringe is produced at point $X$ in figure~\ref{fig:double-slits-x}.}\qpoint{2}

  waves (from slits) overlap (at point $X$) \hfillpoint{1}

  path difference (from slits to $X$) is zero\\*
  \OR phase difference (between the two waves) is zero \hfillpoint{1}\\*
  (so constructive interference gives bright fringe)

  \NOT statements that applies to all bright fringes in general -- e.g. path difference = $n \lambda$ or
  phase difference = $\SI{360}{\degree} n$.
\end{question}

\begin{question}%
  \label{q:double-slits-q2}%
  \qtitle{The intensity of the light passing through the two slits in figure~\ref{fig:double-slits-x} was initially
    the same. The intensity of the light through one of the slits is now reduced. Compare the appearance of the fringes before and after the change of intensity.}\qpoint{2}

  Any \point{2} of:\\*
  same separation/fringe width/number of fringes\\*
  bright fringes/central bright fringe/(fringe at) $X$ less bright\\*
  dark fringes/(fringe at) $Y$/$Z$ brighter

  \NOT `fringes' if it is not clear whether it refers to the dark fringes or the white fringes.
\end{question}

\begin{question}%
  \qtitle{Describe the \refterm{diffraction} of light at a \refterm{diffraction grating}}\qpoint{2}

  waves at the slits \point{1} spread (into the geometric shadow) \point{1}\\*
  \NOT light spread without the word `wave'
\end{question}

\begin{question}%
  \label{q:dffgrat-d-and-i-first}%
  \qtitle{Explain the part played by \refterm{diffraction} and \refterm[superposition]{interference} in the production of the first order maximum by the \refterm{diffraction grating}.}\qpoint{3}

  diffraction: spreading/diverging of waves\st{/light} (takes place) at (each) slit\allowbreak/element\allowbreak/gap\allowbreak/aperture\hfillpoint{1} \\*
  interference: waves (from \refterm{coherent} sources at each slit) overlap \point{1} with phase difference \SI{360}{\degree} / path difference $\lambda$ \point{1}

  See also: question~\ref{q:dffgrat-zero-and-first}
\end{question}

\begin{question}%
  \label{q:dffgrat-zero-and-first}%
  \qtitle{By reference to \refterm[superposition]{interference}, explain the zero and first order maximum in a \refterm{diffraction grating}.}\qpoint{3}

  \begin{itemize}
    \item[zero:] waves (from each slit) overlap/meet/\refterm[superposition]{superpose} \point{1} with a phase difference/path difference of zero \point{1}
    \item[first:] phase difference is \SI{360}{\degree}/path difference of $\lambda$ \hfillpoint{1}
  \end{itemize}

  For the first mark, explicit mentioning that the waves \textbf{meet} or otherwise interference is necessary.

  See also: question~\ref{q:dffgrat-d-and-i-first}
\end{question}

\begin{question}%
  \qtitle{Wave was produced in a ripple tank. Describe how the wave may be observed.}\qpoint{2}

  light above and screen below ripple tank \hfillpoint{1}\\
  ways to ``freeze'' the motion of wave i.e.\ video camera \hfillpoint{1}

  \begin{tikzpicture}[x={(0.9923cm, -0.1240cm)}, y={(-0.2425cm, -0.9701cm)}, z={(-0.1cm, 0.9cm)}]
    \definecolor{water}{HTML}{00e1e1}
    \def\dfoot#1{\draw[thin] #1 -- ++(0, 0, -5)}
    \def\boxline#1{(0, 0, #1) -- (10, 0, #1) -- (10, 5, #1) -- (0, 5, #1) -- (0, 0, #1)}
    % foots
    \dfoot{(0, 0, 0)}; \dfoot{(10, 0, 0)}; \dfoot{(0, 5, 0)}; \dfoot{(10, 5, 0)};

    % screen
    \path[draw=black, thick] (0.6, 0.8, -5) -- (9.1, 0.8, -5) -- (9, 4.5, -5) -- (0.5, 4.5, -5) -- (0.6, 0.8, -5);

    % screen wave
    \foreach \x in {0.5,0.6,...,9} {
      \pgfmathsetmacro\k{sin((\x + 0.7) / 0.8 * 180)}
      \pgfmathsetmacro\o{ifthenelse(\k>=0.5,(\k-0.5)/0.4*0.5,0)}
      \pgfmathsetmacro\dodraw{\o>0}
      \pgfmathsetmacro\xn{\x+0.1}
      \path[opacity=\dodraw, line width=\o, draw=black] (\x, 4.5, -5) -- (\xn, 0.8, -5);
    }

    % box bottom
    \path[fill opacity=0.8, fill=white, draw=black, line width=0.4] \boxline{-0.5};

    % water wave
    \pgfmathsetmacro\dx{0.02}
    \foreach \x in {0,\dx,...,10} {
      \pgfmathsetmacro\s{sin(\x / 0.8 * 180)}
      \pgfmathsetmacro\z{\s * 0.15 - 0.15}
      \pgfmathsetmacro\o{(sin((\x / 0.8 * 180) + 90) + 1) / 2 * 40}
      \pgfmathsetmacro\xn{\x + \dx}
      \pgfmathsetmacro\oo{(\o + 30) / 100}
      \path[fill=black!\o!water, opacity=\oo] (\x, 0, \z) -- (\x, 5, \z) -- (\xn, 5, \z) -- (\xn, 0, \z) -- (\x, 0, \z);
    }

    % vibrator
    \definecolor{wood}{HTML}{ad5a00}
    \path[draw=black, thick, fill=wood] (0.3, 0.2, 0) -- (0.3, 0.2, -0.2) -- (0.3, 4.8, -0.2) -- (0.3, 4.8, 0) -- (0.3, 0.2, 0);
    \path[draw=black, thick, fill=wood!95!black] (0.2, 0.2, 0) -- (0.3, 0.2, 0) -- (0.3, 4.8, 0) -- (0.2, 4.8, 0) -- (0.2, 0.2, 0);
    \path[draw=black, thick, fill=wood!90!black] (0.2, 4.8, 0) -- (0.3, 4.8, 0) -- (0.3, 4.8, -0.2) -- (0.2, 4.8, -0.2) -- (0.2, 4.8, 0);
    \path[draw=black, thin] (0.25, 2.5, 0) -- (0.25, 2.5, 3);
    \path[draw=black, fill=white!50!black, thick] (0, 2.25, 3) -- (0.5, 2.25, 3) -- (0.5, 2.75, 3) -- (0, 2.75, 3) -- (0, 2.25, 3);
    \coordinate (vibrator) at (0.25, 2.5, 3);

    % box side
    \path[fill=white, fill opacity=0.8] (0, 5, 0) -- (0, 5, -0.5) -- (10, 5, -0.5) -- (10, 5, 0) -- (0, 5, 0);
                                        (10, 5, 0) -- (10, 0, 0) -- (10, 0, -0.5) -- (10, 5, -0.5) -- (10, 5, 0);
    \path[line width=0.6, draw=black] (10, 5, 0) -- (10, 5, -0.5);
    \path[line width=0.5, draw=black] (0, 5, 0) -- (0, 5, -0.5);
    \path[line width=0.4, draw=black] (10, 0, 0) -- (10, 0, -0.5);

    % box top
    \path[line width=0.85, draw=black] \boxline{0};

    % lighting
    % \path[fill=yellow, draw=black, thick] (11.14, -1, 4.7) circle[x radius=0.47, y radius=0.6];
    \path[fill=yellow, draw=black, thick, yshift=10em] svg[yscale=-4, xscale=4] {m 75.2 -31.0625 c 0.0119,1.717037 -1.62,3.241493 -1.62,4.83788 0.0331,1.353888 0.5301,3.133954 3.44412,3.534337 2.38242,0.327342 3.68256,-0.745958 3.83885,-3.054577 0.0465,-1.741866 -1.16175,-2.395704 -0.96236,-4.818921 z};
    \path[fill=white, thick, draw=black, yshift=-0.5pt] (10.75, -1, 5) -- (11, -1, 6) -- (12, -1, 6) -- (11.75, -1, 5);
    \path[fill=white, thick, draw=black] (10.75, -1, 5) to[out=-30, in=-150] (11.75, -1, 5);
    \coordinate (light) at (11.5, -1, 6);

    % labels
    \path[<-, thick, draw] (5, 2.5, 0) -- (13, 5, 0) node[pos=1, label={right: water wave}] {};
    \path[<-, thick, draw] (5, 2.5, -5) -- (9, 2, -5) node[pos=1, label={right: wave on screen}] {};
    \node[above=-0.5 of vibrator] {vibrator};
    \node[above=-0.5 of light] {light};
  \end{tikzpicture}
\end{question}

\clearpage

\begin{landscape}
  \thispagestyle{empty}
  \begin{question}%
    \qtitle{The Electromagnetic Spectrum}

    \vspace{3mm}
    \centering\begin{tikzpicture}
      % simplify 2.5sin(exp(0.03x)) * (1-((x-60)/(200-60))*0.9)^0.4
      \draw[thin] plot[id=wavespectrum, samples=8000, domain=60:200, shift={(-10.2, 0)}, xscale=0.17] function{(2.5*(1.38571 - 0.00642857*x)**0.4)*sin(exp(0.03*x))};
      \path[use as bounding box] (0, 5) rectangle (25, -5);
      % \node at (0, 3) {\SI{0}{Hz}};
      % \node at (23.8, 3) {end};
      \begin{scope}[thick, xscale=1.77, yscale=1.3]
      \draw[->] (-0.7, 3) -- (1, 3) node[midway, above] {radio waves} node[pos=1, anchor=north east] {$> \SI{e-1}{m}$};
      \draw[<->] (1, -3) -- (3, -3) node[midway, above] {microwaves} node[pos=0, below] {\SI{e-1}{m}} node[pos=1, below] {\SI{e-3}{m}};
      \node at (1.2, 2) {5G wifi};
      \node at (0.9, 1.5) {2.4G wifi};
      \node at (0.75, 1) {LTE};
      \draw[<->] (3, 3) -- (6, 3) node[midway, above] {infrared} node[pos=0, below] {\SI{e-3}{m}} node[pos=1, below] {\SI{e-6}{m}};
      \draw[<->] (6.1, -3) -- (6.52, -3) node[midway, above, font=\bf] {visible light} node[midway, below] {\SI{600}{nm}-\SI{800}{nm} for red, \textasciitilde\SI{400}{nm} for violet};
      \begin{scope}[yscale=0.7]
        \draw[<->] (6.6, 3) -- (8, 3) node[midway, above] {ultraviolet} node[pos=0, below] {\SI{4e-7}{m}} node[pos=1, below] {\SI{e-8}{m}};
        \draw[<->] (8, -3) -- (13, -3) node[midway, above] {X-rays} node[pos=0, below] {\SI{e-8}{m}} node[pos=1, below] {\SI{e-13}{m}};
        \draw[<-] (10, 3) -- (15, 3) node[midway, above, align=center] {\reftermnoindex[y radiation]{\ce{\gamma}-rays}\index{\ce{\gamma} radiation}} node[pos=0, anchor=north west] {$< \SI{e-10}{m}$};
      \end{scope}
      \end{scope}
    \end{tikzpicture}
    \vspace{3mm}

    The figure shows \refterm{wavelength}. Calculate \refterm{frequency} by $\frac{c}{\text{\refterm{wavelength}}}$. $c$ is the \refterm{speed of light}. The
    wave figure is obviously not to scale.
  \end{question}
\end{landscape}
