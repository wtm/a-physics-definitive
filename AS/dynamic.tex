\defcatagory{Dynamics and Energy}

\begin{question}%
  \qtitle{Define \term{speed}}\qpoint{1}

  $\frac{\text{change in distance}}{\text{change in time}}$\hfillpoint{1}\vspace{0.5em}\\*
  \NOT `change of distance \textit{with} time'
\end{question}

\begin{question}%
  \qtitle{Define \term{velocity}}\qpoint{1}

  rate of change of \refterm{displacement} \OR $\frac{\text{change in displacement}}{\text{time (taken)}}$ \hfillpoint{1}\\*
  \NOT rate of change of displacement per unit time\\*
  \AVOID `displacement over time'\\*
  \NOT `change of displacement \textit{with} time'\\*
  \NOT something with `distance'\\*
  \NOT displacement per second (just like \NOT `meter per time')

  Not to be confused with \refterm{speed}.
\end{question}

\begin{question}%
  \qtitle{Define \term{acceleration}}\qpoint{1}

  rate of change of \refterm{velocity} \OR $\frac{\text{change in \refterm{velocity}}}{\text{time (taken)}}$ \hfillpoint{1}\\*
  \NOT rate of change of velocity per unit time\\*
  \NOT something with `speed'
\end{question}

\begin{question}%
  \qtitle{An object moves in a curved path. Explain why the \refterm{distance} travelled by the object is different from the magnitude of the \refterm{displacement} of the object.}

  distance is the (length of the) curved path, \point{1} while displacement is the minimum / straight line distance \point{1}.
\end{question}

\begin{question}%
  \qtitle{Distinguish between a \refterm{vector} quantity and a \refterm{scalar} quantity.}\qpoint{1}

  Scalar has magnitude only while vector has direction and magnitude \hfillpoint{1}
\end{question}

\begin{question}%
  \label{q:def-force}%
  \qtitle{Define \term{force}}\qpoint{1}

  Rate of change of \term{momentum} \hfillpoint{1}\\*
  \NOT $F = ma$ or ``$\text{\refterm{mass}} \times \text{\refterm{acceleration}}$''\\*
  Definitely \NOT ``a push or pull''
\end{question}

\begin{question}%
  \qtitle{Define \term{power}}\qpoint{1}

  $\frac{\text{\refterm{work} (done)}}{\text{time (taken)}}$ \OR $\frac{\text{\refterm{energy} transferred}}{\text{time (taken)}}$ \OR rate of \refterm{work} done\hfillpoint{1} \vspace{0.5em}\\*
  \NOT $\frac{\text{\refterm{energy}}}{\text{time}}$ \\*
  \AVOID `in a certain time' / `unit of time' \\*
  \AVOID `over time'
\end{question}

\begin{question}%
  \qtitle{Define \term{work} done}\qpoint{2}

  $\text{\refterm{force}} \times \text{distance moved (by force)}$ \hfillpoint{1}\\*
  in the direction of the force \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain what is meant by \term{kinetic energy}.}\qpoint{1}

  energy/ability to do \refterm{work} a object/body/mass has due to its \refterm{speed}\allowbreak/\refterm{velocity}\allowbreak/motion\allowbreak/movement \hfillpoint{1}

  (Formula: $\frac{1}{2}mv^2$)
\end{question}

\begin{question}%
  \qtitle{Define \term{potential energy}}\qpoint{1}

  Stored \refterm{energy} available to do \refterm{work} \hfillpoint{1}\\*
  \NOT description of any specific type of \refterm{energy} e.g. \refterm[gravitational potential energy]{gravitational}

  (Formula: $mgh$)
\end{question}

\begin{question}%
  \qtitle{Define \term{gravitational potential energy}}\qpoint{1}

  \reftermnoindex[energy]{Energy}\index{energy} due to height/position of mass \OR distance from mass \OR
  moving mass from one point to another.\\*
  \OR energy stored when mass moved \hfillpoint{1}\\*
  \NOT about `height of a body above the Earth'\\*
  \NOT about gravitational potential
\end{question}

\begin{question}%
  \qtitle{Define \term{electric potential energy}}\qpoint{1}

  Energy due to position of \textbf{charge} (\NOT mass)\\*
  \OR energy stored when charge moved \hfillpoint{1}
\end{question}

\begin{question}%
  \label{q:def-elasitc-energy}%
  \qtitle{Define \term{elastic potential energy}}\qpoint{1}

  \reftermnoindex[energy]{Energy}\index{energy} (stored) due to deformation/stretching/compressing/change in shape/size \hfillpoint{1}\\*
  \NOT `the energy stored in an elastic body' without mentioning deformation\\*
  \NOT any formula

  (Formula given \refterm{Hooke's law} satisfied and within \refterm{elastic limit}: $\frac{1}{2} \times \text{extension} \times \text{final force}$;
  generally, area under $F/\text{extension}$ graph (such as Figure~\ref{fig:fx-graph}).)
\end{question}


\begin{question}%
  \qtitle{State \term{Hooke's law}.}\qpoint{1}

  \refterm{force}/load is proportional to extension/compression (provided \reftermnoindex[elastic limit]{proportionality limits} not exceeded) \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Define the \term{Young modulus}}\qpoint{1}

  $\frac{\text{\refterm{stress}}}{\text{\refterm{strain}}}$ \hfillpoint{1}
\end{question}

\begin{figure}[h]%
  \figureruletop

  \centering\begin{tikzpicture}
    \coordinate (topright) at (100mm, 100mm);
    \draw[help lines, color=gray!50, thin] (0, 0) grid[step=2mm] (topright);
    \draw[help lines, color=gray!80, thin] (0, 0) grid[step=10mm] (topright);
    \path[draw, ->, thick] let \p1 = (topright) in (0, 0) -- (\x1, 0) -- ++(2mm, 0) node [pos=1, right] {$\frac{x}{\SI{}{cm}}$};
    \path[draw, ->, thick] let \p1 = (topright) in (0, 0) -- (0, \y1) -- ++(0, 2mm) node [pos=1, above] {$\frac{F}{\SI{}{N}}$};
    \foreach \y [evaluate=\y as \yN using \y/10] in {0,10,...,90} {\path[xshift=-1mm, thick, draw, x={(1mm, 0)}, y={(0, 1mm)}] (0, \y) -- ++(2, 0) node[left, pos=0] {\pgfmathprintnumber{\yN}};}
    \foreach \x [evaluate=\x as \xcm using \x/10] in {0,10,...,90} {\path[yshift=-1mm, thick, draw, x={(1mm, 0)}, y={(0, 1mm)}] (\x, 0) -- ++(0, 2) node[below, pos=0] {\pgfmathprintnumber{\xcm}};}
    \path[draw, thick] (0, 0) -- (100mm, 90mm);
  \end{tikzpicture}

  \caption{Figure for question~\ref{q:def-elasitc-energy} and~\ref{q:fx-graph}}
  \label{fig:fx-graph}

  \figureruletbottom
\end{figure}

\begin{question}%
  \label{q:fx-graph}%
  \qtitle{Use data from \textit{<some $F / x\ \text{(extension)}$ graph>} (figure~\ref{fig:fx-graph}) to show that the spring obeys \refterm{Hooke's law} for this range of extensions / compression.}\qpoint{2}

  two values of $F/x$ are calculated which are the same\\*
  \OR ratio of two forces and the ratio of the corresponding two extensions are calculated which are the same\\*
  \OR gradient of graph line calculated and coordinates of one point on the line used with straight line equation $F = mx + c$ to show $c = 0$ \hfillpoint{1}

  (so) \refterm{force} is proportional to extension (and so \refterm{Hooke's law} obeyed) \hfillpoint{1}

  \NOT straight line $\Rightarrow$ \refterm{Hooke's law} obeyed, since line must cross origin.
\end{question}

\begin{question}%
  \qtitle{Describe how to determine whether the extension of the spring is \termnoindex[elastic spring]{elastic}\index{elastic spring}.}\qpoint{1}\\*
  \OR\\*
  \qtitle{State how you would check that the spring has not exceeded its \term{elastic limit}}\qpoint{1}

  remove the \refterm{force}/masses and if the spring returns to its original length its an elastic extension. \hfillpoint{1}\\*
  \NOT something about \reftermnoindex[Hooke's law]{the extension being proportional to the force}.
\end{question}

\begin{question}%
  \qtitle{Explain how the \refterm{elastic limit} may be determined for a spring.} \qpoint{2}

  Add \textbf{small} mass (to cause extension) then remove the mass to see if the spring returns to its original length \hfillpoint{1}

  (If yes,) repeat for larger masses (in small increment) and note the minimum mass for which the spring can't return to its original length \hfillpoint{1}

  \NOT see if force and extension are proportional.
\end{question}

\begin{question}%
  \qtitle{For a ball falling through liquid, state and explain the changes in energy that occur.}\qpoint{2}

  decrease in \refterm{gravitational potential energy} due to decrease in height (since $E_p = mgh$)\\*
  increase in \refterm{thermal energy} due to \refterm{work} done against \refterm{viscous drag}\\*
  loss/change of (total) $E_p$ equal to gain/change in \refterm{thermal energy}\\*
  Any \point{2} points.
\end{question}

\begin{figure}[h]
  \figureruletop

  \vspace{1mm}
  \centering\begin{tikzpicture}[baseline=(ground)]
    \path[draw, thick] (0, 0) -- (12, 0) node[midway] (ground) {};
    \foreach \x in {0.1,0.2,...,11.9} {
      \path[draw=white!50!black, thin] (\x, 0) -- ($(\x, -0.3) + (-0.2, 0)$);
    }
    \path[draw, fill=black!20!white, thick] (1, 0) rectangle (3, 2);
    \path[draw, thick] (3, 1) -- (4, 1)
      foreach \k in {0,...,19} {svg[xscale=18, yscale=-10] {c 0.00272305,-0.18369824 0.0151926,-0.37686922 0.10843037,-0.53989708 0.0295757,-0.0495142 0.0843462,-0.10034872 0.14662436,-0.083658 0.0731027,0.0228236 0.10787242,0.0995136 0.13299077,0.16560054 0.0660851,0.18464345 0.0634247,0.38413255 0.056943,0.57753243 -0.003777,0.0761257 -0.0116582,0.15294259 -0.0330887,0.22618457 -0.014688,0.0347295 -0.0592648,0.0717605 -0.0962404,0.0447865 -0.032086,-0.0344619 -0.0381668,-0.0851831 -0.0454613,-0.13002425 -0.0105278,-0.0864823 -0.0114705,-0.1735277 -0.0115614,-0.26052472}}
      -- (9, 1);
    \path[draw, thick, fill=black!80!white] (9, 2) rectangle (11, 0);
  \end{tikzpicture}

  \caption{Figure for question~\ref{q:box-dragged-by-string}}
  \label{fig:box-dragged-by-string}

  \figureruletbottom
\end{figure}

\begin{question}%
  \label{q:box-dragged-by-string}%
  \qtitle{A box on a horizontal frictionless surface is attached to an elastic spring as in figure~\ref{fig:box-dragged-by-string}, and is
    initially held in rest and the spring is extended. It is then released and as the spring contracts it starts moving towards right.
    Sketch a graph to show how the \refterm{momentum} of the block $p$ varies with time $t$ until the extension of the spring becomes zero. Numerical values are not
    required.}\qpoint{2}

  Answer:

  \centering\begin{tikzpicture}
    \path[->, thick, draw] (0, 0) -- (0, 5) node[midway, label=left:{$p$}] {};
    \path[->, thick, draw] (0, 0) -- (7, 0) node[pos=1, label=below:{$t$}] {};
    \path[draw] (0, 0) .. controls (0.5, 1) and (3, 4) .. (6, 4);
    \node[anchor=north east] at (0, 0) {$0$};
  \end{tikzpicture}

  curve starts from origin and has decreasing gradient \hfillpoint{1}\\*
  final gradient of graph line is zero \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{State the principle of \term{conservation of momentum} (linear momentum)}\qpoint{2}

  Sum/total \refterm{momentum} is constant \OR before = after \hfillpoint{1}\\*
  for an \term{isolated system} \OR with no (resultant) external force \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain what is meant by particles colliding \termnoindex[elastic collision]{elastically}\index{elastic collision}}\qpoint{1}

  the total \refterm{kinetic energy} before (the collision) is equal to the total \refterm{kinetic energy} after (the collision) \hfillpoint{1}

  (Sidenote: an elastic collision has the property that relative speed remain unchanged, i.e.~$v_1 - v_2 = u_2 - u_1$. An non-elastic collision,
  in which some \refterm{kinetic energy} are converted into some other forms of \refterm{energy}, don't has this property.)
\end{question}

\begin{question}%
  \qtitle{Define \term{strain}}\qpoint{1}

  $\frac{\text{extension}}{\text{\textbf{original} length}}$ \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{\termnoindex[stress]{Stress}\index{stress}\ldots}

  Quantity: $\frac{\text{\refterm{force}}}{\text{\textbf{cross-sectional} area}}$ \hfillpoint{1}

  Unit: \SI{}{Pa} = $\frac{\text{\refterm{force}}}{\text{area}}$ \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain the term \term{ultimate tensile stress}}\qpoint{2}

  The maximum force per \textbf{original} cross-sectional area \point{1} that a wire is able to support before it breaks. \point{1}

  \NOT maximum \refterm{stress}
\end{question}

\begin{question}%
  \qtitle{State the two conditions for a system/object to be in \term{equilibrium}}\qpoint{2}

  resultant \refterm{force} (in any direction) is zero \hfillpoint{1}\\*
  resultant \refterm{torque}/\refterm{moment} (about any point) is zero \OR sum of clockwise moment and sum of anticlockwise moment is zero \hfillpoint{1}\\*
  \NOT `there are no turning effect'\\*
  \NOT `the forces are balanced'/`cancel'\\*
  \NOT `no forces acting'
\end{question}

\begin{question}%
  \qtitle{Define the \term{torque} of a \refterm{couple}}\qpoint{2}

  Torque is the product of one of the \refterm[force]{forces} ($F$) and the perpendicular distance ($d$) between forces.

  $\text{One of the forces} \times \text{distance}$ \point{1}\\*
  Perpendicular \point{1}

  \begin{tikzpicture}
    \coordinate (force vector) at (1, 2);
    \coordinate (origin A) at (0, 0);
    \coordinate (origin B) at ($(origin A)!1!-90:($(origin A) + (force vector)$)$);

    \path[->, draw, line width=0.1em] (origin A) -- ++(force vector) node[label=left:$F$, pos=1] {};
    \path[->, draw, line width=0.1em] (origin B) -- ++($-1*(force vector)$) node[label=right:$F$, pos=1] {};
    \path[draw=gray, dashed, thin] ($(origin A)!-2em!(origin B)$) -- ($(origin B)!-2em!(origin A)$);
    \path[draw=theme, |<->|, thick] (origin A) -- (origin B) node[midway, label={[theme]above:$d$}] {};

    \path[draw] let \p0 = (origin A) in let \p1 = ($(\p0)!0.8em!($(\p0) + (force vector)$)$), \p2 = ($(origin A)!0.8em!(origin B)$) in
      (\p1) -- ($(\p1) + (\p2) - (\p0)$) -- (\p2);
  \end{tikzpicture}
\end{question}

\begin{question}%
  \qtitle{Define the \term{moment} of a \refterm{force}}\qpoint{1}

  $\text{\refterm{force}}\ ($F$) \times \text{\textbf{perpendicular} distance}\ ($d$)\\* \text{(of line of action of force) to/from a point / pivot}$ \hfillpoint{1}

  \begin{tikzpicture}
    \coordinate (force origin) at (6, 0);
    \coordinate (force vector) at (0.5, 1.5);
    \path[fill=gray, draw=black, thick] (-1mm, -1mm) rectangle ($(1mm, 1mm) + (force origin)$);
    \path[->, draw, line width=0.1em] (force origin) -- ++(force vector) node[pos=1, label=left:{$F$}] {};
    \coordinate (prep intersect) at ($($(force origin) + (force vector)$)!(0, 0)!(force origin)$);
    \path[dashed, thin, draw] ($(prep intersect)!-1em!(force origin)$) -- (force origin);
    \path[thick, draw=theme, >={Triangle[fill=theme]}, |<->|] (prep intersect) -- (0, 0) node[midway, label={[theme]below:{$d$}}] {};

    % Right angle mark
    \path[draw] let \p0 = (prep intersect) in let \p1 = ($(\p0)!0.8em!(0, 0)$), \p2 = ($(\p0)!0.8em!(force origin)$) in
      (\p1) -- ($(\p1) + (\p2) - (\p0)$) -- (\p2);
  \end{tikzpicture}
\end{question}

\begin{question}%
  \qtitle{Explain what is meant by \term{centre of gravity}}\qpoint{1}

  the point from where (all) the weight (of a body) seems/can be considered to act \hfillpoint{1}\\*
  \NOT weight concentrated on this point\\*
  \NOT the point where mass acts
\end{question}

\begin{question}%
  \qtitle{Define \term{pressure}}\qpoint{1}

  $\frac{\text{\refterm{force}}}{\text{area (normal to the force)}}$ \hfillpoint{1}\\*
  \NOT `cross-sectional area'
\end{question}

\begin{question}%
  \qtitle{Explain the origin of \term{upthrust} for a body in liquid}\qpoint{2}

  Pressure / force up on bottom \textbf{greater} than pressure / force down on top \hfillpoint{2}

  \point{1} for pressure on bottom \textbf{different} from pressure on top \OR pressure changes with depth.\\*
  \NOT having less density than the liquid.
\end{question}

\begin{question}%
  \qtitle{State	Newton's $n$th law of motion\ldots}

  \begin{itemize}
    \item \termnoindex[newton 1st]{$n=1$}\index{Newton's first law}: a body/mass/object continues (at rest or) at constant/uniform \refterm{velocity} unless acted on by a \textbf{resultant} \refterm{force} \hfillpoint{1}\\*
      \NOT `constant speed' without mentioning straight line motion\\*
      \NOT `uniform motion'

    \item \termnoindex[newton 2nd]{$n=2$}\index{Newton's second law}: See definition of \refterm{force} (question \ref{q:def-force})

    \item \termnoindex[newton 3rd]{$n=3$}\index{Newton's third law}: When body A exerts a \refterm{force} on body B,
      force on body A (from B) is equal in magnitude to force on body B (from A) \point{1} , in opposite directions \point{1} , of the same kind. \point{1}

      \OR

      When body A exerts a \refterm{force} on body B, body A also experiences a force of the same kind \point{1}, the same magnitude \point{1} and in opposite direction \point{1}.
  \end{itemize}
\end{question}

\begin{question}%
  \qtitle{Explain how the collision of two objects can support \refterm[newton 3rd]{Newton's third law}}\qpoint{2}

  change in \refterm{momentum} equal (and opposite) for the two objects \hfillpoint{1}

  \refterm{force} is change in \refterm{momentum} over time and time (of collision) is the same\\*
  hence force on the two objects are equal and opposite as for \refterm[newton 3rd]{Newton's third law}. \hfillpoint{1}

  (If you think about it, the law of \refterm{conservation of momentum} can be derived from \refterm[newton 3rd]{Newton's third law}:
  \refterm[newton 3rd]{Newton's third law} means that any \reftermnoindex[force]{forces}\index{force} must come in equal-but-opposite pair, and because they are the result of the same action
  these forces persist for the same time. This means that the change in \refterm{momentum} caused by one \refterm{force} is the opposite of the change in \refterm{momentum} caused by another \refterm{force},
  hence there is no resultant change in \refterm{momentum}. This means that \reftermnoindex[conservation of momentum]{momentum will always be conserved}.)
\end{question}

\def\someball{\textit{<some ball or stuff>}}%
\def\somefloor{\textit{<some floor, wall or stuff>}}%
\begin{question}%
  \qtitle{State and explain whether \reftermnoindex[conservation of momentum]{momentum is conserved}\index{conservation of momentum} during the collision of \someball{} with \somefloor{}}\qpoint{2}

    there is a change/gain in momentum of \somefloor{} \hfillpoint{1}\\*
    \st{(although I highly doubt that, I perfer saying that it is an open system.)}\\*
    there is an equal (and opposite) change to the momentum of \someball{} so momentum is conserved \hfillpoint{1}

    \OR
    
    change of (total) momentum of \somefloor{} and \someball{} is zero\\*
    \OR (total) momentum of \someball{} and \somefloor{} before is equal to the (total) momentum after \hfillpoint{1}\\*
    so momentum is conserved \hfillpoint{1}

    \NOT not conserved for any reason such as an open system.

    (Note that because the question says state ``whether'' the momentum is conserved, not ``explain why'' the momentum is conserved,
    you must make a conclusion in your answer, by e.g.\ adding ``so momentum is conserved''.)
\end{question}

\begin{question}%
  \qtitle{In practice, \refterm{air resistance} is not negligible. State and explain the effect of \refterm{air resistance} on the time taken for a ball thrown upward to reach maximum height.}\qpoint{2}

  deceleration is greater/resultant force (\refterm{weight} and \refterm{friction} force) is greater \hfillpoint{1}\\*
  take less time \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Use the \term{kinetic model} to explain the \refterm{pressure} exerted by gases to wall of container}\qpoint{3}

  molecule collides with wall/container and when they collide there is a change in \refterm{momentum} \hfillpoint{1}

  $\frac{\text{change in \refterm{momentum}}}{\text{time}}$ is \refterm{force} \OR $\Delta p = Ft$. \hfillpoint{1}\vspace{0.5em}

  many/all/sum of molecular collisions (collision forces) over surface/area of container produces (a average force and hence) pressure \hfillpoint{1}
\end{question}

\begin{question}%
  \qtitle{Explain the effect of an increase in \refterm{density}, at constant temperature, on the \refterm{pressure} of a gas.}\qpoint{1}

  More collisions per unit time / higher \refterm{frequency} of collision so greater \refterm{pressure}. \hfillpoint{1}

  Must mention something about the \refterm{frequency} of collision.
\end{question}
